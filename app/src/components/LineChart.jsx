import React, { useEffect, useRef, useState } from 'react';
import Plot from 'react-plotly.js';
import UploadFileIcon from '@mui/icons-material/UploadFile';
import { Button, Select, MenuItem, Typography, TextField } from '@mui/material';

const LineChart = () => {
  const [data, setData] = useState([]);
  const [currentPage, setCurrentPage] = useState(0);
  const [selectedFile, setSelectedFile] = useState('');
  const [linesPerPage, setLinesPerPage] = useState(60000);
  const [totalPages, setTotalPages] = useState(1);
  const fileReaderRef = useRef(null);

  const layout = {
    title: 'ECG Analyzer',
    xaxis: { title: 'X' },
    yaxis: { title: 'Y' },
  };

  const handleFileChosen = async (file) => {
    if (file && file.name) {
      const startIdx = currentPage * linesPerPage;
      const endIdx = startIdx + linesPerPage;
      const blobSlice = file.slice(startIdx, endIdx);
      const blob = new Blob([blobSlice]);
      const contents = await readFileSlice(blob);

      const newLineIdx = contents.indexOf('\n');
      const adjustedStartIdx = startIdx + newLineIdx + 1;
      const adjustedBlobSlice = file.slice(adjustedStartIdx, endIdx);
      const adjustedBlob = new Blob([adjustedBlobSlice]);
      const adjustedContents = await readFileSlice(adjustedBlob);

      const lines = adjustedContents.split('\n');
      const newData = lines.reduce((acc, line) => {
        const parts = line.split(',');
        if (parts.length >= 2 && !isNaN(parseFloat(parts[0])) && !isNaN(parseFloat(parts[1]))) {
          const x = parseFloat(parts[0]);
          const y = parseFloat(parts[1]);
          acc.push([x, y]);
        }
        return acc;
      }, []);

      setTotalPages(Math.ceil(file.size / linesPerPage));
      setData(newData);
    }
  };

  const readFileSlice = (blobSlice) => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.onload = (event) => {
        resolve(event.target.result);
      };
      reader.onerror = (event) => {
        reject(event.target.error);
      };
      reader.readAsText(blobSlice);
    });
  };

  useEffect(() => {
    const handleFileInputChange = async (event) => {
      const file = event.target.files[0];
      if (file) {
        setCurrentPage(0);
        setSelectedFile(file.name);
        handleFileChosen(file);
      }
    };

    if (fileReaderRef.current) {
      fileReaderRef.current.addEventListener('change', handleFileInputChange);
    }

    return () => {
      if (fileReaderRef.current) {
        fileReaderRef.current.removeEventListener('change', handleFileInputChange);
      }
    };
  }, []);

  const handleNextPage = () => {
    const nextPage = currentPage + 1;
    setCurrentPage(nextPage < totalPages ? nextPage : currentPage);
  };

  const handlePageSelect = (event) => {
    setCurrentPage(parseInt(event.target.value, 10));
  };

  useEffect(() => {
    if (currentPage !== 0) {
      const file = fileReaderRef.current?.files[0];
      if (file) {
        handleFileChosen(file);
      }
    }
  }, [currentPage]);

  const handlePrevPage = () => {
    setCurrentPage((prevPage) => Math.max(prevPage - 1, 0));
  };

  const handleLinesPerPageChange = () => {
    setCurrentPage(0);
    const file = fileReaderRef.current?.files[0];
    if (file) {
      handleFileChosen(file);
    }
  };

  return (
    <div className="line-chart-container">
      <Button
        style={{
          display: 'flex',
          justifyContent: 'center',
          flexDirection: 'column'
        }}
        variant="contained" component="label"
      >
        Selecciona un archivo
        <UploadFileIcon />
        <input type="file" ref={fileReaderRef} hidden onChange={handleFileChosen} />
      </Button>
      {
        selectedFile && (
          <Typography style={{ marginTop: '8px' }} variant="subtitle1" gutterBottom>
            Archivo seleccionado: {selectedFile}
          </Typography>
        )
      }
      <Plot
        style={{ marginLeft: 'auto', marginRight: 'auto' }}
        data={[
          {
            type: 'scatter',
            mode: 'lines',
            x: data.map(([x, _]) => x),
            y: data.map(([_, y]) => y),
          },
        ]}
        layout={layout}
        className="line-chart"
      />
      {
        selectedFile && (
          <div>
            <div>
              <Button
                style={{ marginRight: '8px' }}
                variant="contained" onClick={handlePrevPage} disabled={currentPage === 0}>
                Página anterior
              </Button>
              <Select value={currentPage} onChange={handlePageSelect} disabled={totalPages === 0}>
                {Array.from(Array(totalPages), (_, index) => (
                  <MenuItem key={index} value={index}>
                    Página {index + 1}
                  </MenuItem>
                ))}
              </Select>
              <Button
                style={{ marginLeft: '8px' }}
                variant="contained"
                onClick={handleNextPage}
                disabled={currentPage === totalPages - 1}
              >
                Siguiente página
              </Button>
            </div>
            <div style={{ marginTop: '8px' }}>
              <TextField
                label="Tamaño"
                type="number"
                value={linesPerPage}
                onChange={(event) => setLinesPerPage(parseInt(event.target.value, 10))}
                InputProps={{
                  inputProps: {
                    min: 200,
                  },
                }}
              />
              <Button style={{ marginLeft: '8px' }} variant="contained" onClick={handleLinesPerPageChange}>
                Actualizar gráfico
              </Button>
            </div>
          </div>
        )
      }
    </div >
  );
};

export default LineChart;
