import React from 'react';
import LineChart from './components/LineChart';
import HeaderBar from './components/HeaderBar';

const App = () => {

  return (
    <div>
      <HeaderBar />
      <LineChart />
    </div>
  );
};

export default App;