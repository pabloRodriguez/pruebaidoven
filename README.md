# pruebaIdoven

Para abordar eficientemente un proyecto en el que es necesario leer un fichero con una cantidad considerable de datos, he utlizado paginación para agilizar la lectura. He tomado la decisión de utilizar Vite.js como plataforma de desarrollo. Vite.js se destaca por su rapidez y eficiencia en la compilación y ejecución del código, lo que lo convierte en una elección ideal para este tipo de pruebas. Veamos las razones detrás de esta decisión y cómo se complementa con las demás tecnologías seleccionadas.

Vite.js utiliza una arquitectura de servidor de desarrollo rápido y la técnica de módulos ESM para lograr un tiempo de compilación ultrarrápido. Esta característica es especialmente relevante al trabajar con archivos de datos grandes, ya que minimiza los tiempos de espera entre los cambios en el código y su visualización en el navegador. Con Vite.js, podrás disfrutar de un entorno de desarrollo altamente eficiente que optimizará el proceso de carga y procesamiento de los datos.

La implementación de paginación para leer el fichero por bloques también se beneficia de la velocidad de Vite.js. La carga y recarga instantánea de módulos permite establecer un sistema de paginación dinámico y eficiente usando Blobs. Se podrán cargar únicamente los bloques de datos necesarios en cada momento, lo que reducirá significativamente el tiempo de carga inicial y agilizará la navegación y manipulación de los datos.

En cuanto a la elección de Material-UI como librería para componentes estilizados, esta opción brinda una amplia variedad de componentes predefinidos basados en el lenguaje de diseño Material Design de Google. Material-UI facilita la creación de una interfaz de usuario atractiva y coherente.

Por otro lado, se ha seleccionado React-Plotly.js como la biblioteca para renderizar los datos en forma de gráficos interactivos. Esta biblioteca ofrece una poderosa herramienta para visualizar los datos de manera efectiva (zoom, ampliar...). Y para proporcionar mayor flexibilidad al usuario, se ha incluido un input que permite ajustar el tamaño del gráfico visible en pantalla. Esto brinda al usuario la capacidad de personalizar la experiencia de visualización de acuerdo a sus necesidades y preferencias.

En resumen, he elegido Vite.js como plataforma de desarrollo para aprovechar su velocidad y eficiencia en la compilación y ejecución del código. La capacidad de carga y recarga instantánea de módulos de Vite.js se adapta perfectamente al requerimiento de paginación para leer el fichero de datos. Complementado con Material-UI para componentes estilizados y React-Plotly.js para la visualización de datos, se tiene a disposición una herramienta potentes y flexible para desarrollar una aplicación web eficiente y altamente funcional cuando el mayor requirimiento es optimización y funcionalidad.

El núcleo del proyecto y la funcionalidad se encuentra en LineChart.jsx. Ahí se encuentra toda la gestión de la lectura de ficheros grandes, paginación y renderizado.

Para lanzar el proyecto en local:
- cd app/
- npm install
- npm run dev
- o (esto abre una pestaña en el explorador que se tenga abierto en http://localhost:5173/, donde se encontrará la aplicación)